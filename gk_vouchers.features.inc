<?php
/**
 * @file
 * gk_vouchers.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gk_vouchers_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_reward_type().
 */
function gk_vouchers_default_reward_type() {
  $items = array();
  $items['voucher_reward'] = entity_import('reward_type', '{ "type" : "voucher_reward", "label" : "Voucher", "description" : "" }');
  return $items;
}
