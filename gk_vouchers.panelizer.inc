<?php
/**
 * @file
 * gk_vouchers.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_vouchers_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:voucher:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'voucher';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '22dfc922-92ea-4b29-9425-85c78d8138c4';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ed2a92d2-6a4d-4e05-ae4f-ccdb3508cc6a';
    $pane->panel = 'bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_vouchers-gk_vouchers_user_register_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ed2a92d2-6a4d-4e05-ae4f-ccdb3508cc6a';
    $display->content['new-ed2a92d2-6a4d-4e05-ae4f-ccdb3508cc6a'] = $pane;
    $display->panels['bottom'][0] = 'new-ed2a92d2-6a4d-4e05-ae4f-ccdb3508cc6a';
    $pane = new stdClass();
    $pane->pid = 'new-4eaf04d6-905d-4346-a846-cd1a287425d1';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4eaf04d6-905d-4346-a846-cd1a287425d1';
    $display->content['new-4eaf04d6-905d-4346-a846-cd1a287425d1'] = $pane;
    $display->panels['content'][0] = 'new-4eaf04d6-905d-4346-a846-cd1a287425d1';
    $pane = new stdClass();
    $pane->pid = 'new-0559a40f-1333-4902-8171-f1b4a61f1158';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0559a40f-1333-4902-8171-f1b4a61f1158';
    $display->content['new-0559a40f-1333-4902-8171-f1b4a61f1158'] = $pane;
    $display->panels['content_header'][0] = 'new-0559a40f-1333-4902-8171-f1b4a61f1158';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:voucher:default'] = $panelizer;

  return $export;
}
