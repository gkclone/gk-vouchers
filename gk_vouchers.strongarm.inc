<?php
/**
 * @file
 * gk_vouchers.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gk_vouchers_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_referrals_voucher_reward_enabled';
  $strongarm->value = 1;
  $export['gk_referrals_voucher_reward_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_create_account_redirect_path_voucher_reward';
  $strongarm->value = 'user/referrals';
  $export['gk_rewards_create_account_redirect_path_voucher_reward'] = $strongarm;

  return $export;
}
