<?php

/**
 * @file
 * Theme functions for the Vouchers module.
 */

/**
 * Preprocess function for the voucher mail template.
 */
function template_preprocess_gk_vouchers_voucher_mail(&$variables) {
  $voucher = $variables['voucher'];
  $user_details = $variables['user_details'];

  $variables['intro'] = t("Hello !first_name. Here's your voucher...", array(
    '!first_name' => $user_details->first_name,
  ));

  $offer = field_get_items('reward', $voucher, 'field_voucher_reward_offer');
  $variables['offer'] = $offer[0]['value'];

  $summary = field_get_items('reward', $voucher, 'field_voucher_reward_summary');
  $variables['summary'] = $summary[0]['value'];

  $variables['voucher_code'] = $variables['fulfilment']->code;

  // Generate a QR code using Google's chart API.
  if ($qr_code_filepath = gk_core_qr_generate($variables['fulfilment']->code)) {
    $variables['qr_code'] = theme('image', array(
      'path' => file_create_url($qr_code_filepath),
    ));
  }

  $terms_and_conditions = field_get_items('reward', $voucher, 'field_voucher_reward_tsandcs');
  $variables['terms_and_conditions'] = $terms_and_conditions[0]['value'];
}
